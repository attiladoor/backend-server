CREATE TABLE Patients (
    PatientID SERIAL primary key,
    first_name varchar(30) NOT NULL,
    second_name varchar(30) NOT NULL,
    birthdate date NOT NULL
);

CREATE TABLE Doctors (
    DoctorID SERIAL primary key,
    first_name varchar(30) NOT NULL,
    second_name varchar(30) NOT NULL
);

CREATE TABLE Results (
    ResultID SERIAL primary key NOT NULL,
    result_type varchar(30),
    result_text text,
    PatientID integer references Patients(PatientID)
);

CREATE TABLE Permission (
    ResultID integer references Patients(PatientID),
    DoctorID integer references Doctors(DoctorID)
);
