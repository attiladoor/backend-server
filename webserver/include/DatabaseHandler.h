#ifndef DATABASEHANDLER_H_
#define DATABASEHANDLER_H_
#include <pqxx/pqxx> 
#include <cstring>
#include <iostream>
#include <json/json.h>

using namespace std;

#define EmtpyReturn TDatabaseHandlerReturn();

struct TDatabaseHandlerConfig {
    string db_name = "hospital_db";
    string db_user = "postgres";
    string db_userpw = "postgres";
    string hostaddr = "127.0.0.1";
    int port = 5432;
};

class TDatabaseHandlerReturn {

    string  stringRetval;
    bool    isSuccess;

    public:
        TDatabaseHandlerReturn(string r): stringRetval(r), isSuccess(true)  { }
        TDatabaseHandlerReturn(): isSuccess(false) {}
        string getStringReturn() {
            return stringRetval;
        }
        bool getIsSuccessful() {
            return isSuccess;
        }

};

class TDatabaseHandler {

    public:
        TDatabaseHandler(TDatabaseHandlerConfig config_struct);
        ~TDatabaseHandler();
        TDatabaseHandlerReturn ExecuteQuerry(string command, bool isTransactional);

    private:
        bool is_connected = false;
        pqxx::connection* db_connect = NULL; 
        TDatabaseHandlerReturn ExecuteTransactionalQuerry(string command);
        TDatabaseHandlerReturn ExecuteNonTransactionalQuerry(string command);
        static TDatabaseHandlerReturn PQResultToJson(const pqxx::result* R);
};

#endif
