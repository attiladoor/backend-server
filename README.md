# Hospital server
In this training i would like make a backend server with a postgreSQL database for storing doctor and patients data with the corresponding permissions to the result of the patients. 


## Building devenv

The project posseses many various dependencies and configuring a system can be long and rough, wise to use docker container technology.
How to install docker: https://docs.docker.com/install/

If you have succesfully installed Docker, open a terminal and:
```bash
docker pull attiladoor/devenv
```
or you can build your own image by:
``` bash
cd hospital-server/devenv
docker build -t attiladoor/hospital-server .
```
When you run the image, pass your local folder path as an arugment and define the mounting point inside the container:
```bash
docker run -t -i --privileged -v <path>:/data attiladoor/hospital-server /bin/bash
```
If you want to skip Docker and build it own your own host OS, open *hospital-server/devenv/Dockerfile* and install

## Store and Restore DB
If you have already have a configured database, you can save it to a text file. In the example let's name our database to 
*hospital_db* which is owned by *postgres* user.:
```bash
sudo pg_dump -U postgres hospital_db > hospital.db
```

In order to restore the database, first you need to create it and then restore values.
```bash
sudo createdb -U postgres hospital_db
sudo psql -U postgres hospital_db < hospital.db
```

